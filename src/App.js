import logo from './logo.svg';
import './App.css';
import Ex_Mini_Redux from './Ex_Mini_Redux/Ex_Mini_Redux';

function App() {
  return (
    <div className="App">
      <Ex_Mini_Redux />
    </div>
  );
}

export default App;

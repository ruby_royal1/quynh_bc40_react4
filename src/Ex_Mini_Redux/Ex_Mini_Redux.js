import { type } from '@testing-library/user-event/dist/type'
import React, { Component } from 'react'
import { connect } from 'react-redux'

export class Ex_Mini_Redux extends Component {
    render() {

        return (
            <div>
                <button className='btn btn-primary'
                    onClick={this.props.handleMinus}
                >-</button>
                <strong>{this.props.soLuong}</strong>
                <button className='btn btn-success'
                    onClick={this.props.handlePlus}                >+</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        soLuong: state.numberReducer.number
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handlePlus: () => {
            let action = {
                type: "TANG_SO_LUONG"
            }
            dispatch(action)
        },
        handleMinus: () => {
            let action = {
                type: "GIAM_SO_LUONG"
            }
            dispatch(action)
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Ex_Mini_Redux)
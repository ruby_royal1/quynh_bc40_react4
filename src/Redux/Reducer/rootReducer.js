import React, { Component } from 'react'
import { combineReducers } from 'redux'
import { numberReducer } from './numReducer'

export const rootReducer = combineReducers({
    numberReducer: numberReducer
})
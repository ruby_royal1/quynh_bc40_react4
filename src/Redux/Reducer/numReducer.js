const initialState = {
    number: 1
}

export const numberReducer = (state = initialState, action) => {
    switch (action.type) {

        case "TANG_SO_LUONG": {
            state.number++
            return { ...state }
        }
        case "GIAM_SO_LUONG": {
            state.number--
            return { ...state }
        }

        default:
            return state
    }
}
